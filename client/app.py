import sys
from chatterbot import ChatBot
from commands import ListBotsCommand, StartSessionCommand, EndSessionCommand, Command
from models.bots import VCOSBot, SlackBot
from cli import CLI
from programs import CommandsProgram, Program
import models.bots


class CLIHandler(Command):
    def __init__(self, app):
        self.app = app

    def run(self, text):
        text = text.strip()
        if self.app.session is None:
            return "No bot selected. @tellme list"
        else:
            return self.app.session.chatbot.get_response(text).text

class TellmeProgram(CommandsProgram):
    def __init__(self, app):
        super(TellmeProgram, self).__init__()
        self.app = app
        self.add("start_session", StartSessionCommand(self.app))
        self.add("end_session", EndSessionCommand(self.app))

        @self.command("list")
        def list_bots(self):
            return "\n".join((bot.name for bot in models.bots.all()))

class Session(object):
    def __init__(self, bot, chatbot):
        self.bot = bot
        self.chatbot = chatbot

class App(object):
    def __init__(self):
        self.session = None
        self.cli = CLI(CLIHandler(self))
        self.cli.add("@tellme", TellmeProgram(self))

    def run(self):
        while True:
            try:
                print "> ",
                self.cli.interpret(sys.stdin, sys.stdout)
            except(KeyboardInterrupt, EOFError, SystemExit):
                break

    def start_session(self, bot):
        chatbot = ChatBot(
            bot.name,
            #input_adapter="adapters.BotInputAdapter",
            #output_adapter="adapters.BotOutputAdapter"
            logic_adapters=[
                {
                    "import_path": "adapters.BotAdapter",
                    "bot": bot
                }
            ]
        )
        self.session = Session(bot, chatbot)

    def end_session(self):
        if self.session is None:
            return False
        self.session = None
        return True

    def has_session(self):
        return self.session is not None

    def get_session(self):
        return self.session
