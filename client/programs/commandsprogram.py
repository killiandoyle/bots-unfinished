import re
import sys
from .program import Program

def parse(text):
    words = re.split(r'\s+', text.strip())
    name = ""
    if len(words) > 0:
        name = words[0]
    arg = text[len(name):].strip()
    return (name, arg)

class CommandsProgram(Program):

    def __init__(self):
        self.commands = {}

    def add(self, name, command):
        self.commands[name] = command
        return self

    def command(self, name):
        '''Decorator way to add a command'''
        def outer(func):
            self.commands[name] = func
            def inner(*args):
                func(*args)
            return inner
        return outer

    def on_invalid_input(self, text):
        return "Invalid command. Please select one of {}".format(" ".join(self.commands.keys()))

    def run(self, text):
        (name, arg) = parse(text)
        if name not in self.commands:
            return self.on_invalid_input(text)
        command = self.commands[name]
        if callable(command):
            return command(arg)
        else:
            return command.run(arg)
