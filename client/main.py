from app import App
import models.bots
from models.bots import VCOSBot, SlackBot

app = App()

if __name__ == "__main__":
    models.bots.add(VCOSBot("HAL 9000", "localhost", port="3001"))
    models.bots.add(VCOSBot("Wal-E", "localhost", port="3002"))
    models.bots.add(VCOSBot("Agent Smith", "localhost", port="3003"))
    #models.bots.add(SlackBot("Slackbot"))
    app.run()
