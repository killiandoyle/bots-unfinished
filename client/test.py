import unittest
from chatterbot.conversation import Statement
from bots import Bot
import models.bots
from commands import Command
from commands import ListBotsCommand

class DummyBot(Bot):
    pass

class DummyCommand(Command):
    def __init__(self, value):
        self.value = value

    def call(self, arg):
        return self.value, arg
