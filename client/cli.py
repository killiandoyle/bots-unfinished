import re
from commands import Command

class DefaultCommand(Command):
    def __init__(self, cli):
        self.cli = cli

    def run(self, text):
        return "Invalid command. Valid commands are {}".format(self.cli.list_commands())

class CLI(object):

    def __init__(self, default_command):
        self.commands = {}
        self.default_command = default_command or DefaultCommand(self)

    def add(self, name, command):
        self.commands[name] = command

    def interpret(self, in_stream, out_stream):
        text = in_stream.readline().strip()
        words = re.split(r"\s+", text)
        name = words[0]
        command = self.commands.get(name, self.default_command)
        arg = text
        if name in self.commands:
            arg = text[len(name):].strip()
        output = command.run(arg)
        out_stream.write(output)
        out_stream.write("\n")

    def list_commands(self):
        return self.commands.values()
