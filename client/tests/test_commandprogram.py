import unittest
from programs.commandsprogram import CommandsProgram

class OutStream(object):
    def __init__(self):
        self.contents = ""

    def write(self, something):
        self.contents += something

class TestProgram(CommandsProgram):
    def on_invalid_command(self, name):
        return name

class DummyCommand(object):
    def run(self, arg, out):
        out.write("command " + arg)

class TestCommandProgram(unittest.TestCase):
    def test_test(self):
        p = TestProgram()

        outstream = OutStream()
        @p.command("list")
        def listy(name, out):
            out.write("list " + name)
        p.run("list stuff", outstream)
        self.assertEqual(outstream.contents, "list stuff")

        outstream = OutStream()
        p.add("command", DummyCommand())
        p.run("command arg", outstream)
        self.assertEqual(outstream.contents, "command arg")
