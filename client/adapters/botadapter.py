from chatterbot.logic import LogicAdapter
from chatterbot.conversation import Statement
import requests

class BotAdapter(LogicAdapter):

    def __init__(self, **kwargs):
        super(BotAdapter, self).__init__(**kwargs)
        self.bot = kwargs.get("bot")

    def can_process(self, statement):
        return True

    def process(self, statement):
        return Statement(self.bot.send(statement.text))
