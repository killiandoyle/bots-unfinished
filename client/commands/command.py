class Command(object):

    def run(self, text):
        raise NotImplementedError()
