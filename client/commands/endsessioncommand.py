from .command import Command

class EndSessionCommand(Command):

    def __init__(self, session_controller):
        self.session_controller = session_controller

    def run(self, text):
        self.session_controller.end_session()
        return "Session has been ended"

    