from .command import Command
import models.bots

class ListBotsCommand(Command):

    def run(self, text):
        bots = models.bots.all()
        return "Please connect to one of the following bots. {}".\
            format(" ".join((bot.name for bot in bots)))
