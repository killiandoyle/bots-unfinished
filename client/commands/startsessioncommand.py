from .command import Command
import models.bots
from models.bots import NotFoundException

class StartSessionCommand(Command):

    def __init__(self, session_controller):
        self.session_controller = session_controller

    def run(self, name):
        name = name.strip()
        try:
            bot = models.bots.get(lambda bot: bot.name == name)
            self.session_controller.start_session(bot)
            return "Session started"
        except NotFoundException:
            return "Bot {} not found".format(name)
        