class Bot(object):

    def __init__(self, name):
        self.id = None
        self.name = name

    def send(self, text):
        raise NotImplementedError()
