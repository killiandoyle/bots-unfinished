from bot import Bot
import requests

class VCOSBot(Bot):

    def __init__(self, name, host, port="8080"):
        super(VCOSBot, self).__init__(name)
        self.host = host
        self.port = port

    def send(self, text):
        response = requests.get("http://{host}:{port}/askmeanything".\
            format(host=self.host, port=self.port), data={"q": text})
        return response.json()["response"]
