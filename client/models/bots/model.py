import threading, uuid

BOTS = []

class NotFoundException(Exception): pass
class AlreadyExistsException(Exception): pass


def add(bot):
    if bot.id is not None: raise ValueError("Invalid argument given")
    if bot in BOTS: raise AlreadyExistsException()
    BOTS.append(bot)
    bot.id = uuid.uuid1()

def remove(bot):
    BOTS.pop(BOTS.index(bot))

def get(func):
    for bot in BOTS:
        if func(bot):
            return bot
    raise NotFoundException()

def all():
    return tuple(tuple(BOTS))
