# -*- coding: utf-8 -*-

import json, os, sys, time, uuid, math, random
import requests
from flask import Flask, request
from requests import ConnectionError


app = Flask(__name__)

responses = [
    "I wouldn't do that Dave",
    "Howdie doodie",
    "What's up, doc.",
    "Scooby Dooby Doo!",
    "Yabba dabba doo",
    "I tawt I taw a puddy tat"

]

@app.route("/askmeanything")
def ask_me_anything():
    answer = responses[random.randint(0, len(responses) - 1)]
    return json.dumps({'response': answer})

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080, debug=True)
